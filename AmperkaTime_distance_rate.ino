#define SPEED_LEFT      6
#define SPEED_RIGHT     5
#define DIR_LEFT        7
#define DIR_RIGHT       4

void go(int speed, bool reverseLeft, bool reverseRight, int duration)
{
    analogWrite(SPEED_LEFT, speed);
    analogWrite(SPEED_RIGHT, speed);
    digitalWrite(DIR_LEFT, reverseLeft ? LOW : HIGH);
    digitalWrite(DIR_RIGHT, reverseRight ? LOW : HIGH);
    delay(duration);
}

void setup()
{
    for(int i = 4; i <= 7; ++i)
        pinMode(i, OUTPUT);
}

void loop()
{
    // Задержка 5 секунд после включения питания
    delay(5000);

    for (int i = 200; i <= 1000; i += 100) {
        // Несколько сотен мс вперёд
        go(50, false, false, 200);
        go(0, false, false, 0);

        // Задержка 5 секунд
        delay(5000);
    }

    // Остановка до ресета или выключения питания
    go(0, false, false, 0);

    // Приехали
    while (true)
        ;
}
